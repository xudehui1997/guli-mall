package com.atguigu.guilmall.search;

import com.alibaba.fastjson.JSON;
import com.atguigu.guilmall.search.config.GulimallElasticSearchConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.aggregations.metrics.AvgAggregationBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;

@SpringBootTest()
@Slf4j
public class GulimallSearchApplicationTests {

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Test
    public void searchData() throws IOException {
        // 1. 创建检索请求
        SearchRequest searchRequest = new SearchRequest();
        // 2. 指定索引
        searchRequest.indices("bank");
        // 3. 构造检索条件
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        // 3.1 query 查询
        searchSourceBuilder.query(QueryBuilders.termQuery("address", "mill"));
        // 3.2 按照年龄进行分组聚合
        TermsAggregationBuilder ageTermsAgg = AggregationBuilders.terms("ageAgg").field("age").size(10);
        searchSourceBuilder.aggregation(ageTermsAgg);
        // 3.3 按照平均薪资进行聚合
        AvgAggregationBuilder balanceAvgAgg = AggregationBuilders.avg("balanceAvg").field("balance");  // 计算年龄平均数
        searchSourceBuilder.aggregation(balanceAvgAgg);

        searchRequest.source(searchSourceBuilder);

        // 4. 执行检索
        SearchResponse searchResponse = restHighLevelClient.search(searchRequest, GulimallElasticSearchConfig.COMMON_OPTIONS);

        // 5. 分析检索结果
        System.out.println(searchResponse.toString());
        // 5.1 获取所有的 hits
        SearchHits hits = searchResponse.getHits();
        SearchHit[] searchHits = hits.getHits();
        for (SearchHit searchHit : searchHits) {
            System.out.println(searchHit.toString());
            //  可以使用 JSON 工具将字符串转换为类实体
        }

        // 5.2 获取所有的 aggregations
        Aggregations aggregations = searchResponse.getAggregations();
        Terms ageAgg = aggregations.get("ageAgg");
        List<? extends Terms.Bucket> buckets = ageAgg.getBuckets();
        for (Terms.Bucket bucket : buckets) {
            System.out.println(bucket.getKey());
            System.out.println(bucket.getDocCount());
        }

    }

    @Test
    public void indexData() throws IOException {

        IndexRequest indexRequest = new IndexRequest("users");
        indexRequest.id("1");   //数据的id
        User user = new User("张三", "男", 18);

        // 要保存的内容(JSON 字符串)
        String jsonString = JSON.toJSONString(user);
        indexRequest.source(jsonString, XContentType.JSON);

        // 执行操作
        IndexResponse index = restHighLevelClient.index(indexRequest, GulimallElasticSearchConfig.COMMON_OPTIONS);

        // 提取有用的响应数据
        System.out.println(index);
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    class User {
        private String userName;
        private String gender;
        private Integer age;
    }

}
