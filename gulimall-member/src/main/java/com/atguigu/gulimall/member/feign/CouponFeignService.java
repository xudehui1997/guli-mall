package com.atguigu.gulimall.member.feign;

import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author coderxdh
 * @create 2022-05-04 22:06
 */

@FeignClient("gulimall-coupon")  // 需要调用的远程的服务
public interface CouponFeignService {

    @RequestMapping("/coupon/coupon/test/open/feign")  // 远程服务的方法签名
    public R testOpenFeign();
}
