package com.atguigu.gulimall.ware.service.impl;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.ware.dao.WareSkuDao;
import com.atguigu.gulimall.ware.entity.WareInfoEntity;
import com.atguigu.gulimall.ware.entity.WareSkuEntity;
import com.atguigu.gulimall.ware.feign.ProductFeignService;
import com.atguigu.gulimall.ware.service.WareInfoService;
import com.atguigu.gulimall.ware.service.WareSkuService;
import com.atguigu.gulimall.ware.vo.WareSkuRespVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    @Autowired
    private WareInfoService wareInfoService;

    @Resource
    private ProductFeignService productFeignService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addStock(Long skuId, Long wareId, Integer skuNum) {
        // 判断数据库中是否有这个库存记录，没有则新增后插入，有则在原有基础上增加
        List<WareSkuEntity> wareSkuEntities = baseMapper.selectList(new QueryWrapper<WareSkuEntity>().eq("sku_id", skuId).eq("ware_id", wareId));

        if (wareSkuEntities == null || wareSkuEntities.size() == 0) {
            WareSkuEntity wareSkuEntity = new WareSkuEntity();
            wareSkuEntity.setSkuId(skuId);
            wareSkuEntity.setStock(skuNum);
            wareSkuEntity.setWareId(wareId);
            wareSkuEntity.setStockLocked(0);
            // TODO：远程查询sku的名字，如果失败整个事务无需回滚
            try{
                R info = productFeignService.info(skuId);
                Map<String,Object> data = (Map<String, Object>) info.get("skuInfo");
                if (info.getCode() == 0) {
                    wareSkuEntity.setSkuName((String) data.get("skuName"));
                }
            } catch (Exception e) {

            }
            // 添加库存信息
            baseMapper.insert(wareSkuEntity);
        } else {
            // 修改库存信息
            baseMapper.addStock(skuId,wareId,skuNum);
        }
    }

    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<WareSkuEntity> queryWrapper = new QueryWrapper<>();
        // 没有传递该参数过来则赋值为 ""
        String skuIdStr = params.get("skuId") != null ? params.get("skuId").toString() : "";
        Long skuId;
        // 如果传递过来的参数为 "" 则赋值为 0L
        skuId = skuIdStr.equals("") ? 0L : Long.parseLong(skuIdStr);
        if (!skuId.equals(0L)) {
            queryWrapper.eq("sku_id", skuIdStr);
        }
        String wareIdStr = params.get("wareId") != null ? params.get("wareId").toString() : "";
        Long wareId;
        wareId = wareIdStr.equals("") ? 0L : Long.parseLong(wareIdStr);
        if (!wareId.equals(0L)) {
            queryWrapper.eq("ware_id", wareId);
        }

        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                queryWrapper
        );
        List<WareSkuEntity> wareSkuEntities = page.getRecords();

        List<WareSkuRespVo> wareSkuRespVoList = wareSkuEntities.stream().map(wareSkuEntity -> {
            WareSkuRespVo wareSkuRespVo = new WareSkuRespVo();
            BeanUtils.copyProperties(wareSkuEntity, wareSkuRespVo);
            WareInfoEntity wareInfoEntity = wareInfoService.getById(wareSkuEntity.getWareId());
            wareSkuRespVo.setWareName(wareInfoEntity.getName());
            return wareSkuRespVo;
        }).collect(Collectors.toList());

        PageUtils pageUtils = new PageUtils(page);
        pageUtils.setList(wareSkuRespVoList);

        return pageUtils;
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                new QueryWrapper<WareSkuEntity>()
        );

        return new PageUtils(page);
    }
}