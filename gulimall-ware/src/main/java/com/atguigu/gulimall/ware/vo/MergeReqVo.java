package com.atguigu.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author coderxdh
 * @create 2022-05-20 20:06
 */
@Data
public class MergeReqVo {
    private Long purchaseId;  // 采购单
    private List<Long> items; // 需要合并的采购需求
}
