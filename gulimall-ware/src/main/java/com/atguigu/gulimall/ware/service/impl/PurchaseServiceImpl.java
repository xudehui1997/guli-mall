package com.atguigu.gulimall.ware.service.impl;

import com.atguigu.common.constant.WareConstant;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.ware.dao.PurchaseDao;
import com.atguigu.gulimall.ware.entity.PurchaseDetailEntity;
import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.atguigu.gulimall.ware.service.PurchaseDetailService;
import com.atguigu.gulimall.ware.service.PurchaseService;
import com.atguigu.gulimall.ware.service.WareSkuService;
import com.atguigu.gulimall.ware.vo.MergeReqVo;
import com.atguigu.gulimall.ware.vo.PurchaseDoneVo;
import com.atguigu.gulimall.ware.vo.PurchaseItemDoneVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;


@Service("purchaseService")
public class PurchaseServiceImpl extends ServiceImpl<PurchaseDao, PurchaseEntity> implements PurchaseService {

    @Autowired
    private PurchaseDetailService purchaseDetailService;

    @Autowired
    private WareSkuService wareSkuService;

    @Override
    public void finishPurchase(PurchaseDoneVo doneVo) {
        // 获取采购单Id
        Long id = doneVo.getId();

        // 1. 改变采购项(采购需求)状态
        boolean flag = true;
        List<PurchaseItemDoneVo> items = doneVo.getItems();
        List<PurchaseDetailEntity> updatePurchaseDetailEntities = new ArrayList<>();
        for (PurchaseItemDoneVo item : items) {
            PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
            if (item.getStatus() == WareConstant.PurchaseDetailStatusEnum.EXIST_ERROR_STATUS.getCode()) {
                flag = false;
                purchaseDetailEntity.setStatus(item.getStatus());
            } else {
                purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.COMPLETED_STATUS.getCode());
                // 3. 将成功采购的进行入库
                PurchaseDetailEntity entity = purchaseDetailService.getById(item.getItemId());
                wareSkuService.addStock(entity.getSkuId(), entity.getWareId(), entity.getSkuNum());  // 哪个商品存多少到哪个仓库
            }
            purchaseDetailEntity.setId(item.getItemId());
            updatePurchaseDetailEntities.add(purchaseDetailEntity);
        }
        purchaseDetailService.updateBatchById(updatePurchaseDetailEntities);

        // 2. 根据采购项去改变采购单状态，全部成功则成功
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(id);
        purchaseEntity.setStatus(flag ? WareConstant.PurchaseStatus.COMPLETED_STATUS.getCode() : WareConstant.PurchaseStatus.EXCEPTION_STATUS.getCode());
        purchaseEntity.setUpdateTime(new Date());
        this.updateById(purchaseEntity);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void receivedPurchase(List<Long> purchaseIds) {
        // 1. 确认采购单状态为：新建或已分配
        List<PurchaseEntity> purchaseEntities = this.baseMapper.selectBatchIds(purchaseIds);
        List<PurchaseEntity> realPurchaseEntities = purchaseEntities.stream().filter(purchaseEntity -> {
            return purchaseEntity.getStatus() == WareConstant.PurchaseStatus.NEW_STATUS.getCode() ||
                    purchaseEntity.getStatus() == WareConstant.PurchaseStatus.ASSIGNED_STATUS.getCode();
        }).collect(Collectors.toList());

        // 2. 改变采购单状态为：已领取
        List<PurchaseEntity> updatePurchaseEntities = realPurchaseEntities.stream().map(purchaseEntity -> {
            purchaseEntity.setStatus(WareConstant.PurchaseStatus.RECEIVED_STATUS.getCode());
            return purchaseEntity;
        }).collect(Collectors.toList());
        this.updateBatchById(updatePurchaseEntities);

        // 3. 改变采购需求状态为：正在采购
        updatePurchaseEntities.forEach(purchaseEntity -> {
            List<PurchaseDetailEntity> purchaseDetailEntities = purchaseDetailService.listDetailByPurchaseId(purchaseEntity.getId());
            List<PurchaseDetailEntity> updatePurchaseDetailEntities = purchaseDetailEntities.stream().map(purchaseDetailEntity -> {
                PurchaseDetailEntity entity = new PurchaseDetailEntity();
                // 只需要按照id去更新状态
                entity.setId(purchaseDetailEntity.getId());
                entity.setStatus(WareConstant.PurchaseDetailStatusEnum.BUYING_STATUS.getCode());
                return entity;
            }).collect(Collectors.toList());
            purchaseDetailService.updateBatchById(updatePurchaseDetailEntities);
        });
    }

    @Override
    public void mergePurchase(MergeReqVo mergeReqVo) {
        Long purchaseId = mergeReqVo.getPurchaseId();
        // 1. 没有选中需要合并到的采购单则新建一个
        if (purchaseId == null) {
            PurchaseEntity purchaseEntity = new PurchaseEntity();
            purchaseEntity.setCreateTime(new Date());
            purchaseEntity.setUpdateTime(new Date());
            purchaseEntity.setStatus(WareConstant.PurchaseStatus.NEW_STATUS.getCode());
            this.save(purchaseEntity);
            purchaseId = purchaseEntity.getId();
        }

        // 2. 合并采购需求到采购单 TODO 确认采购单状态是0,1才可以合并
        // 2.1 获取需要合并的采购需求的id
        List<Long> items = mergeReqVo.getItems();
        Collection<PurchaseDetailEntity> purchaseDetailEntities = purchaseDetailService.listByIds(items);
        purchaseDetailEntities.forEach((item) -> {
            if (!item.getStatus().equals(WareConstant.PurchaseDetailStatusEnum.CREATED_STATUS.getCode())
                    && !item.getStatus().equals(WareConstant.PurchaseDetailStatusEnum.ASSIGNED_STATUS.getCode())) {
                throw new IllegalArgumentException("正在采购，无法进行分配");
            }
        });
        // 2.2 更新采购需求：绑定的采购单、状态
        Long finalPurchaseId = purchaseId;
        List<PurchaseDetailEntity> collect = items.stream().map(i -> {
            PurchaseDetailEntity purchaseDetailEntity = new PurchaseDetailEntity();
            purchaseDetailEntity.setId(i);
            purchaseDetailEntity.setPurchaseId(finalPurchaseId);
            purchaseDetailEntity.setStatus(WareConstant.PurchaseDetailStatusEnum.ASSIGNED_STATUS.getCode());
            return purchaseDetailEntity;
        }).collect(Collectors.toList());
        purchaseDetailService.updateBatchById(collect);

        // 3. 更新采购单时间
        PurchaseEntity purchaseEntity = new PurchaseEntity();
        purchaseEntity.setId(purchaseId);
        purchaseEntity.setUpdateTime(new Date());
        this.updateById(purchaseEntity);
    }

    @Override
    public PageUtils queryUnReceivePage(Map<String, Object> params) {
        QueryWrapper<PurchaseEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("status", WareConstant.PurchaseStatus.NEW_STATUS.getCode())
                .or().eq("status", WareConstant.PurchaseStatus.ASSIGNED_STATUS.getCode());
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<PurchaseEntity> page = this.page(
                new Query<PurchaseEntity>().getPage(params),
                new QueryWrapper<PurchaseEntity>()
        );

        return new PageUtils(page);
    }

}