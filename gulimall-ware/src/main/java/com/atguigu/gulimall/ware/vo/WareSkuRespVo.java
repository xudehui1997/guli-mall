package com.atguigu.gulimall.ware.vo;

import lombok.Data;

/**
 * @author coderxdh
 * @create 2022-05-20 17:13
 */
@Data
public class WareSkuRespVo {
    private Long id;

    private Long skuId;

    private Long wareId;

    private Integer stock;

    private String skuName;

    private Integer stockLocked;

    private String wareName;
}
