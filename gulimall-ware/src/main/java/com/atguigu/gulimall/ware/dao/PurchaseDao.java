package com.atguigu.gulimall.ware.dao;

import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-04 19:44:52
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {

}
