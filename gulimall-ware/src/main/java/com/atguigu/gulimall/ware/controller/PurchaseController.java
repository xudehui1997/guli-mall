package com.atguigu.gulimall.ware.controller;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.ware.entity.PurchaseEntity;
import com.atguigu.gulimall.ware.service.PurchaseService;
import com.atguigu.gulimall.ware.vo.MergeReqVo;
import com.atguigu.gulimall.ware.vo.PurchaseDoneVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 采购信息
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-04 19:44:52
 */
@RestController
@RequestMapping("ware/purchase")
public class PurchaseController {
    @Autowired
    private PurchaseService purchaseService;

    /**
     * 完成采购单(不一定是成功完成，该采购单也可能存在异常)
     *
     * @param doneVo 前端对象
     * @return 结果
     */
    @PostMapping("/done")
    public R finishPurchase(@RequestBody PurchaseDoneVo doneVo) {
        purchaseService.finishPurchase(doneVo);
        return R.ok();
    }

    /**
     * 采购人员领取采购单
     *
     * @param purchaseIds 采购单Id
     * @return 结果
     */
    @PostMapping("/received")
    public R receivedPurchase(@RequestBody List<Long> purchaseIds) {
        purchaseService.receivedPurchase(purchaseIds);
        return R.ok();
    }

    /**
     * 合并采购需求到采购单
     *
     * @param mergeReqVo 前端vo
     * @return 结果
     */
    @PostMapping("/merge")
    public R mergePurchase(@RequestBody MergeReqVo mergeReqVo) {
        purchaseService.mergePurchase(mergeReqVo);
        return R.ok();
    }

    /**
     * 获取未分配的采购单
     *
     * @param params 参数
     * @return list
     */
    @RequestMapping("/unreceive/list")
    public R unReceiveList(@RequestParam Map<String, Object> params) {
        PageUtils page = purchaseService.queryUnReceivePage(params);

        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    // TODO：条件查询
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = purchaseService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
        PurchaseEntity purchase = purchaseService.getById(id);

        return R.ok().put("purchase", purchase);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody PurchaseEntity purchase) {
        purchaseService.save(purchase);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody PurchaseEntity purchase) {
        purchaseService.updateById(purchase);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids) {
        purchaseService.removeByIds(Arrays.asList(ids));

        return R.ok();
    }

}
