package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author coderxdh
 * @create 2022-05-19 21:15
 */
@Data
public class SpuBoundTo {

    private Long spuId;

    private BigDecimal buyBounds;

    private BigDecimal growBounds;

}
