package com.atguigu.common.to;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author coderxdh
 * @create 2022-05-19 21:40
 */
@Data
public class MemberPrice {

    private Long id;
    private String name;
    private BigDecimal price;

}
