package com.atguigu.common.exception;

import com.atguigu.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.HashMap;
import java.util.Map;

/**
 * @author coderxdh
 * @create 2022-05-08 16:50
 */
@Slf4j
@Component // Spring 容易自动管理
@RestControllerAdvice
public class UnifiedExceptionHandler {
    /**
     * 处理 BadSqlGrammarException 异常
     *
     * @param e BadSqlGrammarException 异常对象
     * @return R对象
     */
    @ExceptionHandler(BadSqlGrammarException.class)
    public R handleBadSqlGrammarException(BadSqlGrammarException e) {
        log.error(e.getMessage(), e);
        return R.error(BizCodeEnum.BAD_SQL_GRAMMAR__EXCEPTION.getCode(), BizCodeEnum.BAD_SQL_GRAMMAR__EXCEPTION.getMsg());
    }

    /**
     * 处理 MaxUploadSizeExceededException 异常
     *
     * @param e MaxUploadSizeExceededException 异常对象
     * @return R 对象
     */
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public R handleMaxUploadSizeExceededException(MaxUploadSizeExceededException e) {
        log.error(e.getMessage(), e);
        return R.error(BizCodeEnum.UPLOAD_FILE_EXCEED_EXCEPTION.getCode(), BizCodeEnum.UPLOAD_FILE_EXCEED_EXCEPTION.getMsg());
    }


    /**
     * 参数非法（效验参数）异常 MethodArgumentNotValidException
     *
     * @param e 异常
     * @return R对象
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public R handleValidException(MethodArgumentNotValidException e) {
        log.error("数据效验出现问题{},异常类型{}", e.getMessage(), e.getClass());
        BindingResult bindingResult = e.getBindingResult();

        Map<String, String> errMap = new HashMap<>();
        bindingResult.getFieldErrors().forEach((fieldError) -> {
            errMap.put(fieldError.getField(), fieldError.getDefaultMessage());
        });
        return R.error(BizCodeEnum.INVALID_EXCEPTION.getCode(), BizCodeEnum.INVALID_EXCEPTION.getMsg())
                .put("data", errMap);
    }

    /**
     * 处理 controller 上一层的异常，下面列举了有可能出现的异常
     *
     * @param e 异常
     * @return R对象
     */
    @ExceptionHandler({
            NoHandlerFoundException.class,
            HttpRequestMethodNotSupportedException.class,
            HttpMediaTypeNotSupportedException.class,
            MissingPathVariableException.class,
            MissingServletRequestParameterException.class,
            TypeMismatchException.class,
            HttpMessageNotReadableException.class,
            HttpMessageNotWritableException.class,
            HttpMediaTypeNotAcceptableException.class,
            ServletRequestBindingException.class,
            ConversionNotSupportedException.class,
            MissingServletRequestPartException.class,
            AsyncRequestTimeoutException.class
    })
    public R handleServletException(Exception e) {  // 这里使用 Exception 接收，因为不可能把上面的类型都写出来
        log.error(e.getMessage(), e);
        return R.error(BizCodeEnum.SERVLET_EXCEPTION.getCode(), BizCodeEnum.SERVLET_EXCEPTION.getMsg());
    }

    @ExceptionHandler(value = Exception.class)
    public R handleException(Exception e) {
        log.error("错误异常:" + e.getMessage());
        return R.error(BizCodeEnum.UNKNOWN_EXCEPTION.getCode(), BizCodeEnum.UNKNOWN_EXCEPTION.getMsg());
    }
}
