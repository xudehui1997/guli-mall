package com.atguigu.common.constant;

/**
 * @author coderxdh
 * @create 2022-05-12 19:09
 */
public class ProductConstant {

    public enum AttrTypeEnum {

        ATTR_TYPE_BASE(1, "基本属性"),
        ATTR_TYPE_SALE(0, "销售属性");

        private final int code;
        private final String msg;

        AttrTypeEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return this.code;
        }

        public String getMsg() {
            return this.msg;
        }
    }
}
