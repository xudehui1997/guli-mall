package com.atguigu.common.constant;

/**
 * @author coderxdh
 * @create 2022-05-20 19:45
 */
public class WareConstant {
    public enum PurchaseStatus {

        NEW_STATUS(0, "新建"),
        ASSIGNED_STATUS(1, "已分配"),
        RECEIVED_STATUS(2, "已领取"),
        COMPLETED_STATUS(3, "已完成"),
        EXCEPTION_STATUS(3, "有异常"),
        ;

        private final Integer code;
        private final String msg;

        PurchaseStatus(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

        public int getCode() {
            return this.code;
        }

        public String getMsg() {
            return this.msg;
        }
    }
    public enum PurchaseDetailStatusEnum {
        CREATED_STATUS(0, "新建"),
        ASSIGNED_STATUS(1, "已分配"),
        BUYING_STATUS(2, "正在采购"),
        COMPLETED_STATUS(3, "已完成"),
        EXIST_ERROR_STATUS(4, "采购失败"),

        ;

        private int code;

        private String msg;

        public int getCode() {
            return code;
        }

        public String getMsg() {
            return msg;
        }

        PurchaseDetailStatusEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }
    }
}
