package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.RefundInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 退款信息
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-04 19:38:19
 */
@Mapper
public interface RefundInfoDao extends BaseMapper<RefundInfoEntity> {

}
