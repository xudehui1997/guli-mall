package com.atguigu.gulimall.order.dao;

import com.atguigu.gulimall.order.entity.OrderSettingEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单配置信息
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-04 19:38:19
 */
@Mapper
public interface OrderSettingDao extends BaseMapper<OrderSettingEntity> {

}
