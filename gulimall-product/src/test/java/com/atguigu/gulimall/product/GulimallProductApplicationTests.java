package com.atguigu.gulimall.product;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

@Slf4j
@SpringBootTest
class GulimallProductApplicationTests {


    @Autowired
    BrandService brandService;

    @Autowired
    CategoryService categoryService;

    @Test
    public void testGetCatelogPathById() {
        Long[] catelogPath = categoryService.getCatelogPathById(0L);
        log.error("catelogPath：" + Arrays.asList(catelogPath));
    }

    @Test
    void testInsert() {
        BrandEntity brandEntity = new BrandEntity(null, "小米", "yyy", "打造智能生态", 1, "M", 0);
        BrandEntity brandEntity2 = new BrandEntity(null, "华为", "xxx", "中华有为", 1, "H", 0);
        brandService.saveBatch(Arrays.asList(brandEntity, brandEntity2));
    }

    @Test
    void testGetList() {
        List<BrandEntity> brandEntityList = brandService.list(new QueryWrapper<BrandEntity>().eq("name", "小米"));
        brandEntityList.forEach(System.out::println);
    }
}
