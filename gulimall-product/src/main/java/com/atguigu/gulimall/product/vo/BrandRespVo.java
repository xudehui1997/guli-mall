package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author coderxdh
 * @create 2022-05-17 11:32
 */
@Data
public class BrandRespVo {
    private Long brandId;
    private String brandName;
}
