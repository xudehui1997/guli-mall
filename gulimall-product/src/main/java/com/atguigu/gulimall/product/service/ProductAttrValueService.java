package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.product.vo.spu.BaseAttrs;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-04 18:12:47
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveBaseAttrs(Long spuInfoEntityId, List<BaseAttrs> baseAttrs);

    List<ProductAttrValueEntity> baseAttrListForSPU(Long spuId);

    void updateSPUAttr(Long spuId, List<ProductAttrValueEntity> entities);
}

