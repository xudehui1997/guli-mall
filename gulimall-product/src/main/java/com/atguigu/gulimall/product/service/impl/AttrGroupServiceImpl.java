package com.atguigu.gulimall.product.service.impl;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.AttrGroupDao;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.atguigu.gulimall.product.service.AttrGroupService;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.AttrGroupWithAttrsRespVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    @Override
    public List<AttrGroupWithAttrsRespVo> getAttrGroupWithAttrsByCatelogId(String catelogId) {
        // 1. 查询该分类下所以的分组信息
        List<AttrGroupEntity> attrGroupEntities = this.list(new QueryWrapper<AttrGroupEntity>().eq("catelog_id", catelogId));
        if (attrGroupEntities == null || attrGroupEntities.size() == 0) {
            return null;
        }

        // 2. 查询所有分组下的基本属性信息
        List<AttrGroupWithAttrsRespVo> attrGroupWithAttrsRespVoList = attrGroupEntities.stream().map(attrGroupEntity -> {
            AttrGroupWithAttrsRespVo attrGroupWithAttrsRespVo = new AttrGroupWithAttrsRespVo();
            BeanUtils.copyProperties(attrGroupEntity, attrGroupWithAttrsRespVo);
            // 查询每一个分组下的基本属性，并赋值给 AttrGroupWithAttrsRespVo 对象中的属性
            List<AttrEntity> attrEntities = attrService.getAttrByAttrGroupId(attrGroupEntity.getAttrGroupId());
            attrGroupWithAttrsRespVo.setAttrs(attrEntities);
            return attrGroupWithAttrsRespVo;
        }).collect(Collectors.toList());

        return attrGroupWithAttrsRespVoList;
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<AttrGroupEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long catelogId) {
        // 1. 查询条件
        String key = (String) params.get("key");

        // 2. 构造 QueryWrapper
        QueryWrapper<AttrGroupEntity> queryWrapper = new QueryWrapper<>();

        // 3. 如果含有查询条件，则进行将条件封装到 queryWrapper 中
        if (!StringUtils.isNullOrEmpty(key)) {
            queryWrapper.and((Object) -> {
                Object.eq("attr_group_id",key).or().like("attr_group_name",key);
            });
        }

        // 4. 如果 catelogId 为 0，则是查询所有属性列表，否则对这个分类查询属性列表
        // TODO：点击分类后在新增属性时自动填充好级联选择器。目前只是针对分类有属性的情况下才有用，需要补充分类无属性时也能返回 catelogPath
        IPage<AttrGroupEntity> page = null;
        if (catelogId != 0) {
            queryWrapper.eq("catelog_id", catelogId);
             page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    queryWrapper
            );
            page.convert(item -> {
                // 当分类无属性时都无法进行遍历
                Long[] catelogPath = categoryService.getCatelogPathById(catelogId);
                item.setCatelogPath(catelogPath);
                return item;
            });
        } else {
            page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    queryWrapper
            );
        }

        return new PageUtils(page);
    }
}