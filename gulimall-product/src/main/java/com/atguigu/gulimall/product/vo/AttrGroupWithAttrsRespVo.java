package com.atguigu.gulimall.product.vo;

import com.atguigu.gulimall.product.entity.AttrEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

/**
 * @author coderxdh
 * @create 2022-05-17 20:08
 */
@Data
public class AttrGroupWithAttrsRespVo {

    private Long attrGroupId;

    private String attrGroupName;

    private Integer sort;

    private String descript;

    private String icon;

    private Long catelogId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<AttrEntity> attrs;

}
