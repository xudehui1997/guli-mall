package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-12 11:26:23
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {

}
