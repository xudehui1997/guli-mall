package com.atguigu.gulimall.product.service.impl;

import com.atguigu.common.constant.ProductConstant;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.atguigu.gulimall.product.dao.AttrDao;
import com.atguigu.gulimall.product.dao.AttrGroupDao;
import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.AttrAttrgroupRelationEntity;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.AttrReqVo;
import com.atguigu.gulimall.product.vo.AttrRespVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("attrService")
public class AttrServiceImpl extends ServiceImpl<AttrDao, AttrEntity> implements AttrService {

    @Autowired
    private AttrAttrgroupRelationDao attrAttrgroupRelationDao;

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private AttrGroupDao attrGroupDao;

    @Autowired
    private CategoryService categoryService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                new QueryWrapper<AttrEntity>()
        );

        return new PageUtils(page);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveAttr(AttrReqVo attrReqVo) {
        // 1. 保存 attr 到 pms_attr 表中
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attrReqVo, attrEntity);
        this.save(attrEntity);

        // 补充0：attr_type=0 是销售属性，不用保存到关联表中
        if (attrReqVo.getAttrType().equals(ProductConstant.AttrTypeEnum.ATTR_TYPE_SALE.getCode())) {
            return;
        }

        // 补充3：如果添加属性的时候选择属性分组，则不保存到 pms_attr_attrgroup_relation 表中
        if (attrReqVo.getAttrGroupId() == null || attrReqVo.getAttrGroupId() == 0) {
            return;
        }

        // 2. 保存关联关系到 pms_attr_attrgroup_relation 表中
        AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
        attrAttrgroupRelationEntity.setAttrId(attrEntity.getAttrId());  // mp 的功能，插完后可以获取主键
        attrAttrgroupRelationEntity.setAttrGroupId(attrReqVo.getAttrGroupId());
        attrAttrgroupRelationDao.insert(attrAttrgroupRelationEntity);

        // TODO：了解 MP 是怎么实现插入后自动获取主键
    }

    @Override
    public PageUtils queryBaseOrSaleAttrPage(Map<String, Object> params, String attrType, Long catelogId) {
        QueryWrapper<AttrEntity> queryWrapper = new QueryWrapper<>();

        // 补充0：根据 attr_type 决定是获取基本属性还是销售属性
        int attrTypeInt = attrType.equalsIgnoreCase("base") ? ProductConstant.AttrTypeEnum.ATTR_TYPE_BASE.getCode() : ProductConstant.AttrTypeEnum.ATTR_TYPE_SALE.getCode();
        queryWrapper.eq("attr_type", attrTypeInt);

        // 1. 如果传入的分类不为0，则进行分类查询
        if (catelogId != 0) {
            queryWrapper.eq("catelog_id", catelogId);
        }
        // 2. 条件查询
        String key = (String) params.get("key");
        if (!StringUtils.isNullOrEmpty(key)) {
            queryWrapper.and((wrapper) -> {
                wrapper.eq("attr_id", key).or().like("attr_name", key);
            });
        }
        // 3. 进行分页查询
        IPage<AttrEntity> page = this.page(
                new Query<AttrEntity>().getPage(params),
                queryWrapper
        );
        // 4. 对查询到的数据进行额外处理
        PageUtils pageUtils = new PageUtils(page);
        List<AttrEntity> records = page.getRecords();
        List<AttrRespVo> respVoList = records.stream().map(attrEntity -> {
            AttrRespVo attrRespVo = new AttrRespVo();
            BeanUtils.copyProperties(attrEntity, attrRespVo);
            // 4.1 填充 catelogName
            CategoryEntity categoryEntity = categoryDao.selectById(attrEntity.getCatelogId());
            if (categoryEntity != null) {
                attrRespVo.setCatelogName(categoryEntity.getName());
            }

            // 补充1：如果是销售属性，则不用填充 groupName，直接返回
            if (attrEntity.getAttrType().equals(ProductConstant.AttrTypeEnum.ATTR_TYPE_SALE.getCode())) {
                return attrRespVo;
            }
            // 4.2 填充 groupName
            AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = attrAttrgroupRelationDao.selectOne(
                    new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrEntity.getAttrId())
            );
            if (attrAttrgroupRelationEntity != null) {
                Long attrGroupId = attrAttrgroupRelationEntity.getAttrGroupId();
                AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrGroupId);
                // 补充2：需要进行判断
                if (attrGroupEntity != null) {
                    attrRespVo.setGroupName(attrGroupEntity.getAttrGroupName());
                }
            }

            return attrRespVo;
        }).collect(Collectors.toList());

        // 5. 对处理好的 AttrRespVo 列表进行返回
        pageUtils.setList(respVoList);
        return pageUtils;
    }

    @Override
    public AttrRespVo getAttrInfo(Long attrId) {
        // 1. 获取 Attr 的基本信息
        AttrRespVo attrRespVo = new AttrRespVo();
        AttrEntity attrEntity = this.getById(attrId);
        BeanUtils.copyProperties(attrEntity, attrRespVo);

        // 2. 设置分类路径
        Long catelogId = attrEntity.getCatelogId();
        Long[] catelogPath = categoryService.getCatelogPathById(catelogId);
        attrRespVo.setCatelogPath(catelogPath);

        // 3. 设置分类名字
        CategoryEntity categoryEntity = categoryDao.selectById(catelogId);
        if (categoryEntity != null) {
            attrRespVo.setCatelogName(categoryEntity.getName());
        }

        // 补充0：销售属性不用设置属性分组信息
        if (attrEntity.getAttrType().equals(ProductConstant.AttrTypeEnum.ATTR_TYPE_SALE.getCode())) {
            return attrRespVo;
        }

        // 4. 设置属性分组信息
        AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = attrAttrgroupRelationDao.selectOne(
                new QueryWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrEntity.getAttrId())
        );
        if (attrAttrgroupRelationEntity != null) {
            Long attrGroupId = attrAttrgroupRelationEntity.getAttrGroupId();
            AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrGroupId);
            if (attrGroupEntity != null) {
                attrRespVo.setGroupName(attrGroupEntity.getAttrGroupName());
                attrRespVo.setAttrGroupId(attrGroupId);
            }
        }

        return attrRespVo;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateAttr(AttrReqVo attrReqVo) {
        // 1. 更新基本信息
        AttrEntity attrEntity = new AttrEntity();
        BeanUtils.copyProperties(attrReqVo, attrEntity);
        this.updateById(attrEntity);

        // 补充0：销售属性不用进行联表更新
        if (attrEntity.getAttrType() == null || attrEntity.getAttrType().equals(ProductConstant.AttrTypeEnum.ATTR_TYPE_SALE.getCode())) {
            return;
        }

        // 2. 联表更新 attr_group_id
        // 2.1 查询是否有 attr_id 相关记录
        QueryWrapper<AttrAttrgroupRelationEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("attr_id", attrReqVo.getAttrId());
        Integer count = attrAttrgroupRelationDao.selectCount(queryWrapper);

        AttrAttrgroupRelationEntity attrAttrgroupRelationEntity = new AttrAttrgroupRelationEntity();
        attrAttrgroupRelationEntity.setAttrId(attrEntity.getAttrId());
        attrAttrgroupRelationEntity.setAttrGroupId(attrReqVo.getAttrGroupId());
        // 2.2 如果存在则需要更新
        if (count > 0) {
            attrAttrgroupRelationDao.update(
                    attrAttrgroupRelationEntity,
                    new UpdateWrapper<AttrAttrgroupRelationEntity>().eq("attr_id", attrEntity.getAttrId())
            );
        } else {
            // 2.3 如果不存在则需要插入
            attrAttrgroupRelationDao.insert(attrAttrgroupRelationEntity);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<AttrEntity> getAttrByAttrGroupId(Long attrGroupId) {
        // 1. 通过 attrGroupId 到 pms_attr_attrgroup_relation 表中查询所有记录
        QueryWrapper<AttrAttrgroupRelationEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("attr_group_id", attrGroupId);
        List<AttrAttrgroupRelationEntity> relationEntities = attrAttrgroupRelationDao.selectList(queryWrapper);
        if (relationEntities == null || relationEntities.size() <= 0) {
            return null;
        }
        // 2. 获取到所有的属性ID
        List<Long> attrIdList = relationEntities.stream().map(entity -> entity.getAttrId()).collect(Collectors.toList());

        // 3. 通过 attrIdList 到 pms_attr 中查询对应的记录
        if (attrIdList.size() <= 0) {
            return null;
        }
        Collection<AttrEntity> attrEntities = this.listByIds(attrIdList);
        return (List<AttrEntity>) attrEntities;
    }

    /**
     * 获取当前属性分组没有关联的基本属性
     *
     * @param attrGroupId 属性分组ID
     * @param params      查询条件
     * @return PageUtils
     */
    @Override
    public PageUtils getNoRelationAttrByAttrGroupId(Long attrGroupId, Map<String, Object> params) {
        // 1. 获取当前属性分组的所在分类
        AttrGroupEntity attrGroupEntity = attrGroupDao.selectById(attrGroupId);
        Long catelogId = attrGroupEntity.getCatelogId();

        // 2. 获取当前属性分组能够关联的基本属性
        // 2.1 获取当前分类的所有属性分组
        List<AttrGroupEntity> groupEntityList = attrGroupDao.selectList(
                new QueryWrapper<AttrGroupEntity>().eq("catelog_id", catelogId)
        );
        List<Long> attrGroupIdList = groupEntityList.stream().map(AttrGroupEntity::getAttrGroupId).collect(Collectors.toList());

        // 2.2 获取这些属性分组已经关联的所有基本属性
        List<AttrAttrgroupRelationEntity> relationEntityList = attrAttrgroupRelationDao.selectList(
                new QueryWrapper<AttrAttrgroupRelationEntity>().in("attr_group_id", attrGroupIdList)
        );
        List<Long> attrIdList = relationEntityList.stream().map(AttrAttrgroupRelationEntity::getAttrId).collect(Collectors.toList());

        // 2.3 从当前分类的所有基本属性中过滤掉已经被关联的基本属性
        QueryWrapper<AttrEntity> queryWrapper = new QueryWrapper<AttrEntity>()
                .eq("catelog_id", catelogId)
                .eq("attr_type", ProductConstant.AttrTypeEnum.ATTR_TYPE_BASE.getCode());
        if (attrIdList.size() > 0) {
            queryWrapper.notIn("attr_id", attrIdList);
        }

        // 3. 进行分页查询
        String key = (String) params.get("key");
        if (!StringUtils.isNullOrEmpty(key)) {
            queryWrapper.and((wrapper) -> {
                wrapper.eq("attr_id", key).or().like("attr_name", key);
            });
        }
        IPage<AttrEntity> page = this.page(new Query<AttrEntity>().getPage(params), queryWrapper);

        return new PageUtils(page);
    }
}