package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.product.vo.spu.SPUSaveReqVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.SpuInfoEntity;

import java.util.Map;

/**
 * spu信息
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-04 18:12:47
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveSpuInfo(SPUSaveReqVo spuSaveReqVo);

    PageUtils queryPageByCondition(Map<String, Object> params);
}

