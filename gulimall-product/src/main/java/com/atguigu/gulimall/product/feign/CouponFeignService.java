package com.atguigu.gulimall.product.feign;

import com.atguigu.common.to.SpuBoundTo;
import com.atguigu.common.to.SpuDiscountRelatedInfoTo;
import com.atguigu.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author coderxdh
 * @create 2022-05-19 21:17
 */
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    @PostMapping("/coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundTo spuBoundTo);

    @PostMapping("/coupon/skufullreduction/saveSpuDiscountRelatedInfo")
    R saveSpuDiscountRelatedInfo(@RequestBody SpuDiscountRelatedInfoTo spuDiscountRelatedInfoTo);
}
