package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.product.vo.AttrReqVo;
import com.atguigu.gulimall.product.vo.AttrRespVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.AttrEntity;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-12 11:26:23
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveAttr(AttrReqVo attrReqVo);

    PageUtils queryBaseOrSaleAttrPage(Map<String, Object> params, String attrType, Long catelogId);

    AttrRespVo getAttrInfo(Long attrId);

    void updateAttr(AttrReqVo attrReqVo);

    List<AttrEntity> getAttrByAttrGroupId(Long attrGroupId);

    PageUtils getNoRelationAttrByAttrGroupId(Long attrGroupId, Map<String, Object> params);
}

