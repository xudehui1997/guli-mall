package com.atguigu.gulimall.product.service;

import com.atguigu.gulimall.product.vo.AttrAttrGroupRelationReqVo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.gulimall.product.entity.AttrAttrgroupRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 属性&属性分组关联
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-12 11:26:23
 */
public interface AttrAttrgroupRelationService extends IService<AttrAttrgroupRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void deleteRelationById(AttrAttrGroupRelationReqVo[] relationReqVos);

    void addAttrAttrGroupRelation(List<AttrAttrGroupRelationReqVo> relationReqVoList);
}

