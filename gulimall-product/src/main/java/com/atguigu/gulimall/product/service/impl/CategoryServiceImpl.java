package com.atguigu.gulimall.product.service.impl;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.CategoryDao;
import com.atguigu.gulimall.product.entity.CategoryEntity;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("categoryService")
public class CategoryServiceImpl extends ServiceImpl<CategoryDao, CategoryEntity> implements CategoryService {

    @Autowired
    private CategoryDao categoryDao;

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<CategoryEntity> page = this.page(
                new Query<CategoryEntity>().getPage(params),
                new QueryWrapper<>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<CategoryEntity> listWithTree() {
        // 1. 获取所有的菜单数据
        List<CategoryEntity> categoryEntityList = baseMapper.selectList(null);

        // 2. 组装成树形结构
        return categoryEntityList.stream().filter(categoryEntity -> {
            return categoryEntity.getCatLevel() == 1;  // 返回第一级分类
        }).map(menu -> {
            menu.setChildren(getChildren(menu, categoryEntityList));  // 获取第一级分类的子菜单
            return menu;
        }).sorted((menu1, menu2) -> {  // 根据 sort 字段对菜单进行排序
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());  // 将结果收集为一个 List 集合
    }

    @Override
    public void removeWithLogicByIds(List<Long> asList) {
        // 1. TODO：检查当前删除的菜单项，是否在其他地方有被引用

        // 2. 尽量不要使用直接删除，使用逻辑删除
        categoryDao.deleteBatchIds(asList);
    }

    @Override
    public Long[] getCatelogPathById(Long catelogId) {
        List<Long> paths = new ArrayList<>();
        getParent(paths, catelogId);
        return paths.toArray(new Long[0]);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateDetail(CategoryEntity category) {
        // 1. 首先更新本表中的 brand 对象
        baseMapper.updateById(category);
        // 2. 其次判断此次更新是否有更新品牌名，没有的话则跳过，有的话则需要进行同步更新
        if (StringUtils.isNullOrEmpty(category.getName())) {
            return;
        }
        // 3. 同步更新
        categoryBrandRelationService.updateByCategory(category.getCatId(), category.getName());
        // 4. TODO：更新其他关联
    }

    /**
     * 递归获取 catelogId 这个节点的父节点
     *
     * @param paths     存储路径
     * @param catelogId 节点
     */
    private void getParent(List<Long> paths, Long catelogId) {
        CategoryEntity categoryEntity = this.getById(catelogId);
        if (categoryEntity.getParentCid() != 0) {
            getParent(paths, categoryEntity.getParentCid());
        }
        // 递归到最后再加入，保证了最先加入的是最顶层的父节点
        paths.add(catelogId);
    }

    /**
     * 递归获取当前分类的子分类
     *
     * @param currentCategory 当前分类
     * @param allCategory     所有分类
     * @return 当前分类的子分类
     */
    private List<CategoryEntity> getChildren(CategoryEntity currentCategory, List<CategoryEntity> allCategory) {
        List<CategoryEntity> collect = allCategory.stream().filter(categoryEntity -> {
            return categoryEntity.getParentCid().equals(currentCategory.getCatId());
        }).map(categoryEntity -> {
            // 1. 递归查找子菜单
            categoryEntity.setChildren(getChildren(categoryEntity, allCategory));
            return categoryEntity;
        }).sorted((menu1, menu2) -> {
            // 2. 对菜单进行排序
            return (menu1.getSort() == null ? 0 : menu1.getSort()) - (menu2.getSort() == null ? 0 : menu2.getSort());
        }).collect(Collectors.toList());
        return collect;
    }

}