package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.SpuImagesEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu图片
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-04 18:12:47
 */
@Mapper
public interface SpuImagesDao extends BaseMapper<SpuImagesEntity> {

}
