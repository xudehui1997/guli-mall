package com.atguigu.gulimall.product.service.impl;

import com.atguigu.common.to.SpuBoundTo;
import com.atguigu.common.to.SpuDiscountRelatedInfoTo;
import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.dao.SpuInfoDao;
import com.atguigu.gulimall.product.entity.*;
import com.atguigu.gulimall.product.feign.CouponFeignService;
import com.atguigu.gulimall.product.service.*;
import com.atguigu.gulimall.product.vo.spu.*;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Service("spuInfoService")
public class SpuInfoServiceImpl extends ServiceImpl<SpuInfoDao, SpuInfoEntity> implements SpuInfoService {

    @Autowired
    private SpuInfoDescService spuInfoDescService;

    @Autowired
    private SpuImagesService spuImagesService;

    @Autowired
    private ProductAttrValueService productAttrValueService;

    @Resource
    private CouponFeignService couponFeignService;

    @Autowired
    private SkuInfoService skuInfoService;

    @Autowired
    private SkuImagesService skuImagesService;

    @Autowired
    private SkuSaleAttrValueService skuSaleAttrValueService;


    @Override
    public PageUtils queryPageByCondition(Map<String, Object> params) {
        QueryWrapper<SpuInfoEntity> queryWrapper = new QueryWrapper<>();

        String key = (String) params.get("key");
        if (!StringUtils.isEmpty(key)) {
            queryWrapper.and(wrapper -> {
                wrapper.eq("id", key).or().like("spu_name", key).or().like("spu_description", key);
            });
        }
        String status = (String) params.get("status");
        if (!StringUtils.isEmpty(status)) {
            queryWrapper.eq("publish_status", status);
        }
        String catelogId = (String) params.get("catelogId");
        if (!StringUtils.isEmpty(catelogId) && !catelogId.equals("0")) {
            queryWrapper.eq("catalog_id", catelogId);
        }
        String brandId = (String) params.get("brandId");
        if (!StringUtils.isEmpty(brandId) && !catelogId.equals("0")) {
            queryWrapper.eq("brand_id", brandId);
        }

        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                queryWrapper
        );

        return new PageUtils(page);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void saveSpuInfo(SPUSaveReqVo spuSaveReqVo) {

        // 1. 保存 SPU 基本信息：pms_spu_info
        SpuInfoEntity spuInfoEntity = new SpuInfoEntity();
        BeanUtils.copyProperties(spuSaveReqVo, spuInfoEntity);
        spuInfoEntity.setCreateTime(new Date());
        spuInfoEntity.setUpdateTime(new Date());
        this.saveBaseSpuInfo(spuInfoEntity);
        Long spuInfoEntityId = spuInfoEntity.getId();

        // 2. 保存 SPU 的详细描述(长图)：pms_spu_info_desc
        List<String> decript = spuSaveReqVo.getDecript();
        SpuInfoDescEntity spuInfoDescEntity = new SpuInfoDescEntity();
        spuInfoDescEntity.setSpuId(spuInfoEntityId);
        spuInfoDescEntity.setDecript(String.join(",", decript));
        spuInfoDescService.saveSpuInfoDesc(spuInfoDescEntity);

        // 3. 保存 SPU 的图片集：pms_spu_images
        List<String> images = spuSaveReqVo.getImages();
        spuImagesService.saveSpuImages(spuInfoEntity.getId(), images);

        // 4. 保存 SPU 的基本属性：pms_product_attr_value
        List<BaseAttrs> baseAttrs = spuSaveReqVo.getBaseAttrs();
        productAttrValueService.saveBaseAttrs(spuInfoEntityId, baseAttrs);

        // 5. 保存 SPU 的积分信息：sms_spu_bounds
        Bounds bounds = spuSaveReqVo.getBounds();
        SpuBoundTo spuBoundTo = new SpuBoundTo();
        BeanUtils.copyProperties(bounds, spuBoundTo);
        spuBoundTo.setSpuId(spuInfoEntity.getId());
        R r = couponFeignService.saveSpuBounds(spuBoundTo);
        if (r.getCode() != 0) {
            log.error("调用远程服务(gulimall-coupon)保存 SPU 积分信息失败");
        }

        // 6. 保存 SPU 的所有 SKU
        List<Skus> skus = spuSaveReqVo.getSkus();
        if (skus == null || skus.size() == 0) {
            return;
        }
        skus.forEach(sku -> {
            // 6.1 保存 SKU 的基本信息：pms_sku_info
            String defaultImg = "";
            for (Images image : sku.getImages()) {
                if (image.getDefaultImg() == 1) {
                    defaultImg = image.getImgUrl();
                }
            }
            SkuInfoEntity skuInfoEntity = new SkuInfoEntity();
            BeanUtils.copyProperties(sku, skuInfoEntity);
            skuInfoEntity.setBrandId(spuInfoEntity.getBrandId());
            skuInfoEntity.setCatalogId(spuInfoEntity.getCatalogId());
            skuInfoEntity.setSaleCount(0L);
            skuInfoEntity.setSpuId(spuInfoEntity.getId());
            skuInfoEntity.setSkuDefaultImg(defaultImg);
            skuInfoService.saveSkuInfo(skuInfoEntity);
            Long skuInfoEntityId = skuInfoEntity.getSkuId();

            // 6.2 保存 SKU 的图片信息：pms_sku_images
            List<SkuImagesEntity> skuImagesEntities = sku.getImages().stream().map(image -> {
                SkuImagesEntity skuImagesEntity = new SkuImagesEntity();
                skuImagesEntity.setSkuId(skuInfoEntityId);
                skuImagesEntity.setImgUrl(image.getImgUrl());
                skuImagesEntity.setDefaultImg(image.getDefaultImg());
                return skuImagesEntity;
            }).filter(image -> {
                // 返回 true 就是需要，false 就是剔除
                return !StringUtils.isEmpty(image.getImgUrl());
            }).collect(Collectors.toList());
            skuImagesService.saveSkuImages(skuImagesEntities);

            // 6.3 保存 SKU 的销售属性信息：pms_sku_sale_attr_value
            List<Attr> attrs = sku.getAttr();
            List<SkuSaleAttrValueEntity> skuSaleAttrValueEntities = attrs.stream().map(attr -> {
                SkuSaleAttrValueEntity skuSaleAttrValueEntity = new SkuSaleAttrValueEntity();
                BeanUtils.copyProperties(attr, skuSaleAttrValueEntity);
                skuSaleAttrValueEntity.setSkuId(skuInfoEntityId);
                return skuSaleAttrValueEntity;
            }).collect(Collectors.toList());
            skuSaleAttrValueService.saveSaleAttrValues(skuSaleAttrValueEntities);

            // 6.4 保存 SKU 的优惠信息，满减信息：sms_sku_full_reduction、sms_sku_ladder、sms_member_price
            SpuDiscountRelatedInfoTo spuDiscountRelatedInfoTo = new SpuDiscountRelatedInfoTo();
            BeanUtils.copyProperties(sku, spuDiscountRelatedInfoTo);
            spuDiscountRelatedInfoTo.setSkuId(skuInfoEntityId);
            if (spuDiscountRelatedInfoTo.getFullCount() > 0 ||
                    spuDiscountRelatedInfoTo.getFullPrice().compareTo(BigDecimal.ZERO) > 0 ||
                    spuDiscountRelatedInfoTo.getMemberPrice().size() > 0
            ) {
                R r1 = couponFeignService.saveSpuDiscountRelatedInfo(spuDiscountRelatedInfoTo);
                if (r1.getCode() != 0) {
                    log.error("调用远程服务(gulimall-coupon)保存 SKU 积分信息失败");
                }
            }
        });
    }

    private void saveBaseSpuInfo(SpuInfoEntity spuInfoEntity) {
        this.baseMapper.insert(spuInfoEntity);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuInfoEntity> page = this.page(
                new Query<SpuInfoEntity>().getPage(params),
                new QueryWrapper<SpuInfoEntity>()
        );

        return new PageUtils(page);
    }
}