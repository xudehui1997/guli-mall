package com.atguigu.gulimall.product.controller;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.entity.AttrEntity;
import com.atguigu.gulimall.product.entity.AttrGroupEntity;
import com.atguigu.gulimall.product.service.AttrAttrgroupRelationService;
import com.atguigu.gulimall.product.service.AttrGroupService;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.CategoryService;
import com.atguigu.gulimall.product.vo.AttrAttrGroupRelationReqVo;
import com.atguigu.gulimall.product.vo.AttrGroupWithAttrsRespVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 属性分组
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-04 18:12:47
 */
@RestController
@RequestMapping("product/attrgroup")
public class AttrGroupController {
    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private AttrService attrService;

    @Autowired
    private AttrAttrgroupRelationService relationService;

    @GetMapping("/{catelogId}/withattr")
    public R getAttrGroupWithAttrs(@PathVariable("catelogId") String catelogId) {
        List<AttrGroupWithAttrsRespVo> voList = attrGroupService.getAttrGroupWithAttrsByCatelogId(catelogId);
        return R.ok().put("data", voList);
    }

    @PostMapping("/attr/relation/save")
    public R addAttrAttrGroupRelation(@RequestBody List<AttrAttrGroupRelationReqVo> relationReqVoList) {
        relationService.addAttrAttrGroupRelation(relationReqVoList);
        return R.ok();
    }

    @GetMapping("{attrGroupId}/noattr/relation")
    public R getAttrGroupNoRelationAttr(
            @PathVariable("attrGroupId") Long attrGroupId,
            @RequestParam Map<String, Object> params
    ) {
        PageUtils pageUtils = attrService.getNoRelationAttrByAttrGroupId(attrGroupId, params);
        return R.ok().put("page", pageUtils);
    }

    /**
     * 删除属性分组下关联的基本属性
     *
     * @param relationReqVos 需要删除的[属性ID, 分组ID]集合
     * @return 结果
     */
    @PostMapping("/attr/relation/delete")
    public R deleteRelationById(@RequestBody AttrAttrGroupRelationReqVo[] relationReqVos) {
        relationService.deleteRelationById(relationReqVos);
        return R.ok();
    }

    /**
     * 通过 attrGroupId 获取属性分组的所有基本属性
     *
     * @param attrGroupId attrGroupId
     * @return 列表
     */
    @GetMapping("/{attrGroupId}/attr/relation")
    public R getAttrGroupRelationAttr(@PathVariable("attrGroupId") Long attrGroupId) {
        // TODO：是在 AttrGroupServiceImpl 中去调用 AttrService 呢还是直接在这里调用？
        List<AttrEntity> list = attrService.getAttrByAttrGroupId(attrGroupId);
        return R.ok().put("data", list);
    }

    /**
     * 列表
     */
    @RequestMapping("/list/{catelogId}")
    public R list(@RequestParam Map<String, Object> params, @PathVariable("catelogId") Long catelogId) {
        PageUtils page = attrGroupService.queryPage(params, catelogId);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{attrGroupId}")
    public R info(@PathVariable("attrGroupId") Long attrGroupId) {
        AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
        Long catelogId = attrGroup.getCatelogId();
        Long[] catelogPath = categoryService.getCatelogPathById(catelogId);
        attrGroup.setCatelogPath(catelogPath);
        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.save(attrGroup);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AttrGroupEntity attrGroup) {
        attrGroupService.updateById(attrGroup);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] attrGroupIds) {
        attrGroupService.removeByIds(Arrays.asList(attrGroupIds));
        return R.ok();
    }
}
