package com.atguigu.gulimall.product.service.impl;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.Query;
import com.atguigu.gulimall.product.dao.BrandDao;
import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.atguigu.gulimall.product.service.CategoryBrandRelationService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mysql.cj.util.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


@Service("brandService")
@Slf4j
public class BrandServiceImpl extends ServiceImpl<BrandDao, BrandEntity> implements BrandService {

    @Autowired
    private CategoryBrandRelationService categoryBrandRelationService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        String key = (String) params.get("key");
        QueryWrapper<BrandEntity> queryWrapper = new QueryWrapper<>();
        if (!StringUtils.isNullOrEmpty(key)) {
            queryWrapper.like("name", params.get("key")).or()
                    .like("descript", params.get("key")).or()
                    .like("first_letter", params.get("key"));
        }

        IPage<BrandEntity> page = this.page(
                new Query<BrandEntity>().getPage(params),
                queryWrapper
        );
        return new PageUtils(page);
    }

    @Override
    public void removeWithLogicByIds(List<Long> asList) {
        // TODO：设计逻辑删除，但是把 showStatus 设置为逻辑删除字段，那么就无法更新状态，因此需要额外设计一个字段完成逻辑删除
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateDetail(BrandEntity brand) {
        // 1. 首先更新本表中的 brand 对象
        baseMapper.updateById(brand);
        // 2. 其次判断此次更新是否有更新品牌名，没有的话则跳过，有的话则需要进行同步更新
        if (StringUtils.isNullOrEmpty(brand.getName())) {
            return;
        }
        // 3. 同步更新
        categoryBrandRelationService.updateByBrand(brand.getBrandId(), brand.getName());
        // 4. TODO：更新其他关联
    }
}