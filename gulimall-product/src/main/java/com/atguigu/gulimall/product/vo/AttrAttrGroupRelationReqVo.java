package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author coderxdh
 * @create 2022-05-12 22:12
 */
@Data
public class AttrAttrGroupRelationReqVo {
    private Long attrId;
    private Long attrGroupId;
}
