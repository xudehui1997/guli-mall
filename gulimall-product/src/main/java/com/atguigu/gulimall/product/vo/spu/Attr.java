/**
 * Copyright 2022 bejson.com
 */
package com.atguigu.gulimall.product.vo.spu;

import lombok.Data;

/**
 * Auto-generated: 2022-05-19 17:14:0
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
@Data
public class Attr {

    private Long attrId;
    private String attrName;
    private String attrValue;
}