package com.atguigu.gulimall.product.dao;

import com.atguigu.gulimall.product.entity.SpuInfoDescEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * spu信息介绍
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-12 11:26:23
 */
@Mapper
public interface SpuInfoDescDao extends BaseMapper<SpuInfoDescEntity> {

}
