package com.atguigu.gulimall.product.controller;

import com.atguigu.common.utils.PageUtils;
import com.atguigu.common.utils.R;
import com.atguigu.gulimall.product.entity.ProductAttrValueEntity;
import com.atguigu.gulimall.product.service.AttrService;
import com.atguigu.gulimall.product.service.ProductAttrValueService;
import com.atguigu.gulimall.product.vo.AttrReqVo;
import com.atguigu.gulimall.product.vo.AttrRespVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * 商品属性
 *
 * @author codrxdh
 * @email 3313303540@qq.com
 * @date 2022-05-12 11:26:23
 */
@RestController
@RequestMapping("product/attr")
public class AttrController {
    @Autowired
    private AttrService attrService;

    @Autowired
    private ProductAttrValueService productAttrValueService;

    /**
     * 更新 SPU 的规格信息
     *
     * @param spuId    SPU 的 Id
     * @param entities 需要更新的规格信息
     * @return 结果
     */
    @PostMapping("/update/{spuId}")
    public R updateSPUAttr(@PathVariable("spuId") Long spuId,
                           @RequestBody List<ProductAttrValueEntity> entities) {

        productAttrValueService.updateSPUAttr(spuId, entities);
        return R.ok();
    }

    /**
     * 回显 SPU 规格信息
     *
     * @param spuId SPU 的 Id
     * @return 查询结果
     */
    @GetMapping("/base/listforspu/{spuId}")
    public R baseAttrListForSPU(@PathVariable("spuId") Long spuId) {

        List<ProductAttrValueEntity> entities = productAttrValueService.baseAttrListForSPU(spuId);

        return R.ok().put("data", entities);
    }

    /**
     * 列表
     */
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = attrService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 获取基本/销售属性列表
     *
     * @param params    分页查询条件
     * @param catelogId 分类ID
     * @return 列表
     */
    @GetMapping("/{attrType}/list/{catelogId}")
    public R baseOrSaleAttrList(
            @RequestParam Map<String, Object> params,
            @PathVariable("attrType") String attrType,
            @PathVariable("catelogId") Long catelogId
    ) {
        PageUtils page = attrService.queryBaseOrSaleAttrPage(params, attrType, catelogId);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @RequestMapping("/info/{attrId}")
    public R info(@PathVariable("attrId") Long attrId) {
        AttrRespVo attrRespVo = attrService.getAttrInfo(attrId);
        return R.ok().put("attr", attrRespVo);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody AttrReqVo attrReqVo) {
        attrService.saveAttr(attrReqVo);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody AttrReqVo attrReqVo) {
        attrService.updateAttr(attrReqVo);
        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] attrIds) {
        attrService.removeByIds(Arrays.asList(attrIds));
        return R.ok();
    }

}
