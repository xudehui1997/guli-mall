package com.atguigu.gulimall.product.vo;

import lombok.Data;

/**
 * @author coderxdh
 * @create 2022-05-12 13:46
 */
@Data
public class AttrRespVo extends AttrReqVo {

    private String catelogName;  // 该属性所在分类的名字
    private String groupName;  // 该属性所在属性分组的名字

    private Long[] catelogPath; // 分类路径
}
